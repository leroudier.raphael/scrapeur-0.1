const puppeteer = require('puppeteer');

(async () => {
    async function start(url) {
        try {
            const browser = await puppeteer.launch({ headless: true });
            const page = await browser.newPage();
            //recuperation des element sur la page de base
            await page.goto(url);
            const datas = await page.evaluate(() => {
                let temp = { int: null, ext: null };
                let divScore = document.querySelector('#livescore')
                if (divScore) {
                    let divScoreH = document.querySelector('.scoreH')
                    if (divScoreH) temp.int = divScoreH.textContent
                    let divScoreV = document.querySelector('.scoreV')
                    if (divScoreV) temp.ext = divScoreV.textContent
                }
                return temp
            })
            await browser.close();
            return datas
        } catch (err) {
            console.error(err);
        }
    }
    let time = new Date()
    await Promise.all(await find({ date: { $lte: time.toISOString() }, resultat: { $exists: true, $eq: null } }))
        .then(async (matches) => {
            let responses = await getHook(matches)
            // faire le saveAll et verifier lasynchhrone
            await saveAll(await responses)
        })



    async function save(x) {
        return new Promise(async function (resolve, reject) {
            const { MongoClient, ServerApiVersion } = require('mongodb');
            const env = require('dotenv').config().parsed;
            const client = new MongoClient(env.URI_MONGO, { useNewUrlParser: true, useUnifiedTopology: true, serverApi: ServerApiVersion.v1 });
            client.connect(async err => {
                // perform actions on the collection object
                try {
                    const collection = client.db(env.BDD_MONGO).collection(env.BDD_COL);
                    const response = await collection.replaceOne({ _id: x._id }, x, { upsert: false })
                    resolve(response)
                    await client.close();
                } catch (e) {
                    reject(e);
                    await client.close();
                }
            })
        })
    }
    async function getHook(array) {
        return new Promise(async function (res, rej) {
            try {
                for (let index = 0; index < array.length; index++) {
                    array[index].resultat = await start(array[index].url).catch();
                    console.log(await array[index].resultat);
                }
                res(await array)
            } catch (err) {
                rej(err)
            }
        })

    }
    async function saveAll(x) {
        return new Promise(async function (res, rej) {
            try {
                let responses = await x.map(async (y) => {
                    if (y.resultat) {
                        setWinner(y.resultat)
                        return await save(await y)
                    } else {
                        throw 'fuck'
                    }
                })
                res(await responses)
            } catch (err) {
                rej(err)
            }
        })
    }
    function setWinner(resultat) {
        if (resultat && resultat.ext && resultat.int) {
            if (resultat.ext > resultat.int) resultat['value'] = '2'
            else if (resultat.int > resultat.ext) resultat['value'] = '1'
            else resultat['value'] = 'n'
        }
    }
    async function find(params = {}) {
        return new Promise(async function (resolve, reject) {
            const { MongoClient, ServerApiVersion } = require('mongodb');
            let env = require('dotenv').config().parsed;
            const client = new MongoClient(env.URI_MONGO, { useNewUrlParser: true, useUnifiedTopology: true, serverApi: ServerApiVersion.v1 });
            client.connect(async err => {
                const collection = client.db(env.BDD_MONGO).collection(env.BDD_COL);
                // perform actions on the collection object
                try {
                    //findall url exite pas et arrayfilter et save
                    const response = await collection.find(params).limit(30).toArray()
                    resolve(response)
                    await client.close();
                } catch (e) {
                    await client.close();
                    reject(e)
                }

            })
        })
    }
})();