
async function save(params) {
    return new Promise(async function (resolve, reject) {
        const { MongoClient, ServerApiVersion } = require('mongodb');
        let env = require('dotenv').config().parsed;
        const client = new MongoClient(env.URI_MONGO, { useNewUrlParser: true, useUnifiedTopology: true, serverApi: ServerApiVersion.v1 });
        client.connect(async err => {
            const collection = client.db(env.BDD_MONGO).collection(env.BDD_COL);
            // perform actions on the collection object
            try {
                //findall url exite pas et arrayfilter et save
                const response = await collection.insertMany(params)
                console.log(response);
                resolve(response)
                await client.close();
            } catch (e) {
                await client.close();
                reject(e)
            }

        })
    })

}
async function findMatch(params = {}) {
    return new Promise(async function (resolve, reject) {
        const { MongoClient, ServerApiVersion } = require('mongodb');
        let env = require('dotenv').config().parsed;
        const client = new MongoClient(env.URI_MONGO, { useNewUrlParser: true, useUnifiedTopology: true, serverApi: ServerApiVersion.v1 });
        client.connect(async err => {
            const collection = client.db(env.BDD_MONGO).collection(env.BDD_COL);
            // perform actions on the collection object
            try {
                //findall url exite pas et arrayfilter et save
                const response = await collection.find(params).toArray()
                resolve(response)
                await client.close();
            } catch (e) {
                await client.close();
                reject(e)
            }
        })
    })
}
async function findProno(params = {}) {
    return new Promise(async function (resolve, reject) {
        const { MongoClient, ServerApiVersion } = require('mongodb');
        let env = require('dotenv').config().parsed;
        const client = new MongoClient(env.URI_MONGO, { useNewUrlParser: true, useUnifiedTopology: true, serverApi: ServerApiVersion.v1 });
        client.connect(async err => {
            const collection = client.db(env.BDD_MONGO).collection('paris');
            // perform actions on the collection object
            try {
                //findall url exite pas et arrayfilter et save
                const response = await collection.find(params).toArray()
                resolve(response)
                await client.close();
            } catch (e) {
                await client.close();
                reject(e)
            }

        })
    })
}
// find les match qui ont un resultat qui est null
find({ "resultat.value": { $exists: true, $eq: null } }).then(matches => {
    for (let index = 0; index < matches.length; index++) {
        findProno({ idMatch: { $eq: matches[index]._id } }).then(prono => {
            if (prono && matches[index].resultat) {
                setScore(prono, matches[index].resultat)
                matches[index] = await save(matches[index])
            }
        })

    }
})

function setScore(prono, resultat) {
    return prono.predict === resultat.value ? 1 : 0
}
//  find les prono qui corespondent
// les saves