module.exports = class Team {
    constructor(img, name, details = {}) {
        this.img = img
        this.name = name
        this.details = details
    }
}
