class Match {
 
  constructor(cote = [], home = null, outside = null, date = new Date(), location = '', resultat = '', opinions = []) {
    this.cote = cote
    this.home = home
    this.outside = outside
    this.date = new Date(date)
    this.location = location
    this.resultat = resultat
    this.opinions = opinions
  }
}

module.exports = Match
