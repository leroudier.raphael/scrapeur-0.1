const puppeteer = require('puppeteer');

(async () => {
    let urls = [
        'https://www.ruedesjoueurs.com/pronostics/nba-874935.html',
        'https://www.ruedesjoueurs.com/pronostics/foot.html'
    ]
    async function get(adress) {
        const browser = await puppeteer.launch({ headless: true });
        const page = await browser.newPage();
        //recuperation des element sur la page de base
        await page.goto(adress);
        const datas = await page.evaluate(({ adress }) => {
            class Cote {
                constructor(dom = 0, nul = 0, ext = 0) {
                    this._1 = dom
                    this._n = nul
                    this._2 = ext
                }
            }
            class Match {
                constructor(cote = [], home = null, outside = null, url = null, type = null, date = null, location = null, resultat = null, opinions = null) {
                    this.cote = cote
                    this.home = home
                    this.outside = outside
                    this.url = url
                    this.date = date
                    this.location = location
                    this.resultat = resultat
                    this.opinions = opinions
                    this.type = type
                }
            }
            class Team {
                constructor(img, name, details = {}) {
                    this.img = img
                    this.name = name
                    this.details = details
                }
            }

            let array = [];
            let elements = document.querySelector('body > div.habillage > div:nth-child(8) > div.uk-grid.no-margin-mobile > div.uk-width-large-7-10.uk-width-medium-6-10.uk-width-small-1-1.content > section:nth-child(4) > ul')
                ? document.querySelector('body > div.habillage > div:nth-child(8) > div.uk-grid.no-margin-mobile > div.uk-width-large-7-10.uk-width-medium-6-10.uk-width-small-1-1.content > section:nth-child(4) > ul')
                : document.querySelector('body > div.habillage > div:nth-child(8) > div.uk-grid.no-margin-small > div.uk-width-large-7-10.uk-width-medium-6-10.uk-width-small-1-1.content > section:nth-child(4) > ul')
            let spans = elements ? elements.querySelectorAll('.vertical') : null

            if (spans && spans.length > 0) {
                for (const span of spans) {
                    const url = span.parentElement.getAttribute('href')

                    let cotes = span.querySelectorAll('.cote')
                    let imgs = span.querySelectorAll('img')

                    let coteObj = []
                    if (cotes && cotes.length > 0) {
                        for (const cote of cotes) {

                            let cote_content = cote ? cote.querySelectorAll('span') : null
                            if (cote_content) {
                                let cot = {}

                                for (const iter of cote_content) {
                                    cot[iter.className] = iter.textContent ? iter.textContent : null
                                }
                                coteObj.push(cot)
                            }
                        }
                    }
                    let getType = adress.split('/pronostics/')[1]
                    let types = ['foot', 'nba', 'tennis']
                    let typeM
                    types.forEach(element => {
                        if (element[0] === getType[0]) typeM = element
                    });
                    let grey = span.querySelector('.grey')
                    let dd = grey.getAttribute('datetime')
                    let imgAtr = imgs ? imgs[0].getAttribute('data-src') : null
                    let imgAtr2 = imgs ? imgs[1].getAttribute('data-src') : null
                    const cote = coteObj.length > 0 ? new Cote(coteObj[0], coteObj[1], coteObj[2]) : null
                    let xy = span.querySelector('.eq1>span') && span.querySelector('.eq1>span').textContent ? span.querySelector('.eq1>span').textContent : null
                    let xxx = span.querySelector('.eq2>span') && span.querySelector('.eq2>span').textContent ? span.querySelector('.eq2>span').textContent : null
                    if (imgAtr) {
                        imgAtr = imgAtr.split('?small=1')
                    }
                    if (imgAtr2) {
                        imgAtr2 = imgAtr2.split('?small=1')
                    }
                    const int = new Team(imgAtr[0], xy)
                    const ext = new Team(imgAtr2[0], xxx)
                    const match = new Match(cote, int, ext, url, typeM, dd)

                    if (cote) array.push(match)
                }
            }
            return array
        }, { adress })
        await browser.close();
        return datas
    }

    for (let index = 0; index < urls.length; index++) {
        let type = index === 0 ? 'nba' : 'foot'
        const element = urls[index];
        let matches = await get(urls[index])
        await find({ type: type }).then((findall) => {
            findall = findall.map(x => { return x.url })
            if (findall && findall.length > 0 && matches && matches.length > 0) {
                Promise.all(matches.filter(match => !findall.includes(match.url))).then(result => {
                    if (result && result.length > 0)
                        save(result).then(x => console.log(x))
                    else console.log(`Aucun nouveau match de ${type}  mise a jour`)
                })
            } else if (findall && findall.length === 0 && matches && matches.length > 0) {
                save(matches).then(x => console.log(x))
            }
        })
    }

    async function save(params) {
        return new Promise(async function (resolve, reject) {
            const { MongoClient, ServerApiVersion } = require('mongodb');
            let env = require('dotenv').config().parsed;
            const client = new MongoClient(env.URI_MONGO, { useNewUrlParser: true, useUnifiedTopology: true, serverApi: ServerApiVersion.v1 });
            client.connect(async err => {
                const collection = client.db(env.BDD_MONGO).collection(env.BDD_COL);
                // perform actions on the collection object
                try {
                    //findall url exite pas et arrayfilter et save
                    await collection.insertMany(params).then(response => {
                        resolve(response)
                        client.close();
                    })
                } catch (e) {
                    await client.close();
                    reject(e)
                }
            })
        })

    }
    async function find(params = {}) {
        return new Promise(async function (resolve, reject) {
            const { MongoClient, ServerApiVersion } = require('mongodb');
            let env = require('dotenv').config().parsed;
            const client = new MongoClient(env.URI_MONGO, { useNewUrlParser: true, useUnifiedTopology: true, serverApi: ServerApiVersion.v1 });
            client.connect(async err => {
                const collection = client.db(env.BDD_MONGO).collection(env.BDD_COL);
                // perform actions on the collection object
                try {
                    //findall url exite pas et arrayfilter et save
                    const response = await collection.find(params).toArray()
                    resolve(response)
                    await client.close();
                } catch (e) {
                    await client.close();
                    reject(e)
                }

            })
        })
    }
})();
// Optimisation
// il faudrai faire une condition pour qu'il n'enregistre pas de match deja existant : voir avec les filter de mongo a moment du save