const puppeteer = require('puppeteer');
(async () => {
    const fs = require('fs')

    async function get2(url, param = 0) {

        const browser = await puppeteer.launch({ headless: true });
        const page = await browser.newPage();
        //recuperation des element sur la page de base

        await page.goto(url);
        const datas = await page.evaluate(({ param }) => {
            class Opinion {
                constructor(book = [], rdj = [], pers = []) {
                    this.bookmaker = book
                    this.rdj = rdj
                    this.pers = pers
                }
            }
            let temp
            if (param === 1) {
                temp = {
                    int: [],
                    ext: []
                }
                let shadow = document.querySelectorAll('.uk-float-left')
                let shadowF = document.querySelectorAll('.uk-float-right')
                if (shadow && shadow.length > 0) {
                    let shadowL = shadow[0].querySelectorAll('li')
                    shadowF = shadowF && shadowF[0] ? shadowF[0].querySelectorAll('li') : null

                    if (shadowL && shadowL.length > 0) {
                        for (const x of shadowL) {
                            let t = {
                                result: x.style.backgroundColor === 'rgb(73, 190, 73)' ? 'win' : 'lost',
                                score: x.title,
                                color: x.style.backgroundColor === 'rgb(73, 190, 73)' ? 'rgb(73, 190, 73)' : 'red'
                            }
                            temp['int'].push(t)
                        }
                    }

                    if (shadowF && shadowF.length > 0) {
                        for (const x of shadowF) {
                            let t = {
                                result: x.style.backgroundColor === 'rgb(73, 190, 73)' ? 'rgb(73, 190, 73)' : 'red',
                                score: x.title,
                                color: x.style.backgroundColor === 'rgb(73, 190, 73)' ? 'rgb(73, 190, 73)' : 'red'
                            }
                            temp['ext'].push(t)
                        }
                    }
                    let classement = document.querySelectorAll('.blocStat')

                    if (classement && classement.length > 0) {
                        temp['classement_int'] = classement[0]
                            && classement[0].querySelector('.blocStatText')
                            && classement[0].querySelector('.blocStatText').textContent
                            ? classement[0].querySelector('.blocStatText').textContent
                            : null
                        temp['classement_ext'] = classement[1]
                            && classement[1].querySelector('.blocStatText')
                            && classement[1].querySelector('.blocStatText').textContent
                            ? classement[1].querySelector('.blocStatText').textContent
                            : null
                        temp['consecWinExt'] = classement[2]
                            && classement[2].querySelector('.blocStatText')
                            && classement[2].querySelector('.blocStatText').textContent
                            ? classement[2].querySelector('.blocStatText').textContent
                            : null
                        temp['consecWinInt'] = classement[3]
                            && classement[3].querySelector('.blocStatText')
                            && classement[3].querySelector('.blocStatText').textContent
                            ? classement[3].querySelector('.blocStatText').textContent
                            : null
                    }
                }
            } else {
                temp = {
                    opinions: null,
                    imgUrl: null
                }
                let caption = document.getElementById('ViewPay_Opacity')
                if (caption) {
                    let img = caption.querySelector('img')
                    let imgUrl = img ? img.getAttribute('data-src') : null
                    if (imgUrl) temp.imgUrl = imgUrl
                }

                let blocs = document.querySelectorAll('.blocPronoRedacBookMobile')
                if (blocs && blocs.length > 0) {
                    let opinion = new Opinion()
                    let box1 = blocs[0].querySelectorAll('.box1n2')
                    let box2 = blocs[1].querySelectorAll('.box1n2')
                    if (box1 && box1.length > 0) {
                        box1.forEach(box => {
                            if (box.classList.contains('active')) opinion.rdj.push(box.textContent)
                        })
                    } if (box2 && box2.length > 0) {
                        box2.forEach(box => {
                            if (box && box.classList.contains('active')) {
                                opinion.bookmaker.push(box.textContent)
                                opinion.pers = blocs[1] && blocs[1].querySelector('.grey') ? blocs[1].querySelector('.grey').textContent : null
                            }
                        })
                    }
                    temp.opinions = opinion
                }
            }
            return temp
        }, { param })
        await browser.close();

        return datas

    }
    let collection = await Promise.all(await finAllMatch())
    let count = 0
    for (const col of collection) {
        let urlSplit = col.url.split('pronostic')
        const resu = await get2(col.url)
        if (resu) {
            if (resu && resu.opinions) col.opinions = resu.opinions
            if (resu && resu.imgUrl) col['imgUrl'] = resu.imgUrl
            let tempDetail = await get2(urlSplit[0] + 'pronostic/statistiques' + urlSplit[1], 1).catch()
            if (tempDetail) {
                col.outside.details = {
                    consecWin: tempDetail.consecWinExt || null,
                    classement: tempDetail.classement_ext || null,
                    fiveMatch: tempDetail.ext || null
                }
                col.home.details = {
                    classement: tempDetail.classement_int || null,
                    consecWin: tempDetail.consecWinInt || null,
                    fiveMatch: tempDetail.int || null
                }
                
            } if (tempDetail||resu) {
                await save(col).then(() => {
                    count++
                })
            }
        }

    }
console.log(`${count} match modifiés`);

    async function save(x) {
        return new Promise(async function (resolve, reject) {
            const { MongoClient, ServerApiVersion } = require('mongodb');
            const env = require('dotenv').config().parsed;
            const client = new MongoClient(env.URI_MONGO, { useNewUrlParser: true, useUnifiedTopology: true, serverApi: ServerApiVersion.v1 });
            client.connect(async err => {
                // perform actions on the collection object
                try {
                    const collection = client.db(env.BDD_MONGO).collection(env.BDD_COL);
                    const response = await collection.replaceOne({ _id: x._id }, x, { upsert: false })
                    resolve(response)
                    await client.close();
                } catch (e) {
                    reject(e);
                    await client.close();
                }
            })
            console.log('replace reussi ' + x._id.toString());
        })
    }
    async function finAllMatch() {
        return new Promise(async function (resolve, reject) {
            const { MongoClient, ServerApiVersion } = require('mongodb');
            const env = require('dotenv').config().parsed;
            const client = new MongoClient(env.URI_MONGO, { useNewUrlParser: true, useUnifiedTopology: true, serverApi: ServerApiVersion.v1 });
            client.connect(async err => {
                // perform actions on the collection object
                try {
                    const collection = client.db(env.BDD_MONGO).collection(env.BDD_COL);

                    let time = new Date()
                    resolve(await collection.find({ date: { $gte: time.toISOString() }, opinions: { $eq: null } }).toArray())

                } catch (e) {
                    reject(e);
                }
                client.close();
            })
        })
    }
})();
